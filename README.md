These are DAT tapes that contained multiple TITO save sets from
original 7 and 9 track tapes that were destroyed in the early 1990s.

A tool to list and extract files called tito.c can be found here:
https://github.com/larsbrinkhoff/pdp10-its-disassembler
